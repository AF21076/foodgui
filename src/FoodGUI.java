import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.ImageIcon;
import static javax.swing.JOptionPane.showConfirmDialog;

public class FoodGUI  {
    private JPanel root;
    private JButton unadonButton;
    private JButton sobaButton;
    private JButton udonButton;
    private JButton gyudonButton;
    private JButton oyakodonButton;
    private JButton katsudonButton;
    private JTextArea ordered;
    private JButton CheckOut;
    private JButton Reset;
    private JLabel Total;
    int totalprice=0;


    void order (String food,int price){
        int confirmation = showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0){
            JOptionPane.showMessageDialog(null,"Order for "+food+" received. Thank you.");
            totalprice(food,price);
        }
    }

    void totalprice(String food,int price){
        totalprice+=price;
        Total.setText(totalprice+"yen");
        String currentText = ordered.getText();
        ordered.setText(currentText + food+" "+totalprice+"yen\n");
    }

    void Reset(){
        ordered.setText("");
        totalprice = 0;
        String total=String.valueOf(totalprice);
        Total.setText(total+"yen");
    }


    public FoodGUI() {

        unadonButton.setIcon(new ImageIcon(
                this.getClass().getResource("うな丼.png")));
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("そば.png")));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("うどん.png")));
        gyudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("牛丼.png")));
        oyakodonButton.setIcon(new ImageIcon(
                this.getClass().getResource("親子丼.png")));
        katsudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("かつ丼.png")));
        Total.setText("0yen");

        unadonButton.addActionListener(new ActionListener (){
            public void actionPerformed(ActionEvent e) {
                order("Unadon",500);
            }
        });

    sobaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Soba",250);}
    });

    udonButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Udon",300);
        }
    });

    gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Gyudon",400);}
        });

    oyakodonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Oyakodon",350);}
        });

    katsudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Katsudon",450);}
        });

     ordered.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                System.out.print(ordered);
            }
        });

    CheckOut.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to check out?",
                    "Checkout Confirm",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {
                confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to eat in the store?",
                        "Messege",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    totalprice = (int)(totalprice * 1.1);
                    JOptionPane.showMessageDialog(null,
                            "Thank you. Tax rate is 10%. The total is " + totalprice + "yen");
                }
                else {
                    totalprice = (int)(totalprice * 1.08);
                    JOptionPane.showMessageDialog(null,
                            "Thank you. Tax rate is 8%. The total is " + totalprice + "yen");
                }
                Reset();


        }
    }

        });


        Reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to reset ordered food?",
                        "Reset Order",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation==0){
                    JOptionPane.showMessageDialog(null,"Ordered item is reset.");
                    Reset();
                }
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
